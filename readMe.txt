Notifications:
	1) I imagined that I work  in delivering food company and just not believe you that all 5678 restaurants have the same vendor_special_day, 
		so we update schedule from vendor_special_day for each restaurant separately. For 5678 it will not be so costly, so I choose this solution.
	2) I also not believe you that db will not change, so in my solution for all new restaurants schedule will not be restored and we can 
		not take care about all this while we work with last one.
	3) Also I make schedule dumps for each restaurants separately. It is cool for analytics's and if we will not want restore schedule for some of them 
		it will easier make different manipulations.
	4) Technical documentation you can see in  /documentation/index.html you need open this page in browser.


How use:
Set DB configurations in DbProvider  
From project folder run:
21th
php -f script.php dump DecCrisis    !it will create dump of current `vendor_schedule` parameters. You can create as many dumps as you want. DecCrisis is name of dump.
php -f script.php updateFromSpecialDay		!it will update `vendor_schedule` on base `vendor_special_day`.
28th
php -f script.php updateFromDump DecCrisis		!it will update `vendor_schedule` on base dump.
php -f script.php deleteDump DecCrisis		!manually delete dump information.