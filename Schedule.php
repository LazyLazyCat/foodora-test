<?php

/**
 * Class Schedule
 * Base Class for working with `vendor_schedule`.
 */
class Schedule
{
    /**
     * Get array of object for Vendor.
     * @param $id
     * @return array
     */
    public static function getBuyVendorId($id)
    {
        $sth = DbProvider::getInstance()->prepare('SELECT * FROM vendor_schedule WHERE vendor_id = :vendor_id');
        $sth->execute(array(':vendor_id' => $id));

        return $sth->fetchAll(PDO::FETCH_CLASS, 'Schedule');
    }

    /**
     * Save current parameters.
     */
    public function save()
    {
        $sql = "INSERT INTO vendor_schedule(vendor_id,
            weekday,
            all_day,
            start_hour,
            stop_hour) VALUES (
            :vendor_id,
            :weekday,
            :all_day,
            :start_hour,
            :stop_hour)";

        $sth = DbProvider::getInstance()->prepare($sql);

        $sth->bindParam(':vendor_id', $this->vendor_id, PDO::PARAM_INT);
        $sth->bindParam(':weekday', $this->weekday, PDO::PARAM_INT);
        $sth->bindParam(':all_day', $this->all_day, PDO::PARAM_INT);
        $sth->bindParam(':start_hour', $this->start_hour, PDO::PARAM_STR);
        $sth->bindParam(':stop_hour', $this->stop_hour, PDO::PARAM_STR);

        $sth->execute();
    }
}