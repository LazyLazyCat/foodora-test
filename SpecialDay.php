<?php

/**
 * Class SpecialDay
 * Base Class for working with `vendor_special_day`.
 */
class SpecialDay
{

    public static function getBuyVendorId($id)
    {
        $sth = DbProvider::getInstance()->prepare('SELECT * FROM vendor_special_day WHERE vendor_id = :vendor_id');
        $sth->execute(array(':vendor_id' => $id));

        return $sth->fetchAll(PDO::FETCH_CLASS, 'SpecialDay');
    }

    /**
     * Get day of week on base `special_date`
     * @return int
     */
    public function getDayOfWeek()
    {
        return (int)date('N', strtotime($this->special_date));
    }

    /**
     * Convert to Schedule object.
     * @return Schedule
     */
    public function getScheduleObject()
    {
        $Schedule = new Schedule();
        $parameters = get_object_vars($this);
        foreach ($parameters as $key => $value) {
            if ($key != 'id') {
                $Schedule->$key = $value;
            }
        }
        $Schedule->weekday = $this->getDayOfWeek();

        return $Schedule;
    }

}