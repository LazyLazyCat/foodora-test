<?php
include_once 'Schedule.php';
include_once 'SpecialDay.php';

/**
 * Base class for working with `vendor_schedule`. Also include base operations for working with Vendor schedule.
 * Class Vendor
 */
class Vendor
{
    /**
     * Name of dump folder.
     * @var string
     */
    public $dumpFolder = 'dump';
    /***
     * Collection of schedule for current Vendor
     * @var null
     */
    private $Schedules = null;
    /**
     * Collection of schedule for current SpecialDays
     * @var null
     */
    private $SpecialDays = null;

    /**
     * Return all vendors
     * @return array
     */
    public static function getAll()
    {
        $sth = DbProvider::getInstance()->prepare('SELECT * FROM vendor WHERE 1');
        $sth->execute();
        $Vendor = $sth->fetchAll(PDO::FETCH_CLASS, 'Vendor');

        return $Vendor;
    }

    /**
     * Return Schedules use lazy initialization.
     * @return array|null
     */
    public function getSchedules()
    {
        if (is_null($this->Schedules)) {
            $this->Schedules = Schedule::getBuyVendorId($this->id);
        }

        return $this->Schedules;
    }

    /**
     * Return getSpecialDays use lazy initialization.
     * @return array|null
     */
    public function getSpecialDays()
    {
        if (is_null($this->SpecialDays)) {
            $this->SpecialDays = SpecialDay::getBuyVendorId($this->id);
        }

        return $this->SpecialDays;
    }


    /**
     * Dump Schedule.
     * @param string $name
     */
    public function dumpSchedule($name = 'dump')
    {
        $dump = serialize($this->getSchedules());
        file_put_contents($this->getPatch($name), $dump);
    }

    /**
     * Get dumped Schedule.
     * @param string $name
     * @return mixed
     * @throws Exception
     */
    public function unDumpSchedule($name = 'dump')
    {
        $dump = file_get_contents($this->getPatch($name));
        if (!$dump) {
            return array();
        }

        return unserialize($dump);
    }

    /**
     * Delete dump.
     * @param string $name
     */
    public function deleteDump($name = 'dump')
    {
        $directory = $this->dumpFolder . DIRECTORY_SEPARATOR . $name;
        $RecursiveDirectoryIterator = new RecursiveDirectoryIterator($directory, RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator($RecursiveDirectoryIterator, RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($directory);
    }

    /**
     * Get patch name.
     * @param $name
     * @return string
     */
    private function getPatch($name)
    {
        $directory = $this->dumpFolder . DIRECTORY_SEPARATOR . $name;
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }

        return $directory . DIRECTORY_SEPARATOR . $this->id . '.txt';
    }

    /**
     * Update Schedule from dump.
     * @param $name
     * @throws Exception
     */
    public function updateScheduleFromDump($name)
    {
        $Schedules = $this->unDumpSchedule($name);
        $ScheduleDayArray = $this->getDaysScheduleArray($Schedules);
        $this->updateSchedule($ScheduleDayArray);
    }

    /**
     * Update `vendor_schedule` on base `SpecialDays`
     */
    public function updateScheduleFromSpecialDays()
    {
        $ScheduleDayArray = $this->getDaysScheduleArray($this->getSpecialDays());
        $this->updateSchedule($ScheduleDayArray);

    }

    /**
     * Get weekdays array with Schedules.
     * @param $array
     * @return array
     */
    public function getDaysScheduleArray($array)
    {
        $ScheduleDayArray = array();
        foreach ($array as $Schedule) {
            if (get_class($Schedule) == 'SpecialDay') {
                $Schedule = $Schedule->getScheduleObject();
            }
            $ScheduleDayArray[$Schedule->weekday][] = $Schedule;
        }

        return $ScheduleDayArray;
    }

    /**
     * Update Schedule. Manipulation with db.
     * @param $ScheduleDayArray
     */
    private function updateSchedule($ScheduleDayArray)
    {
        foreach ($ScheduleDayArray as $dayIndex => $ScheduleArray) {
            $this->dayOff($dayIndex);
            $this->addSchedules($ScheduleArray);
        }
    }

    /**
     * Set day of.
     * @param $weekday
     */
    public function dayOff($weekday)
    {
        $sql = 'DELETE FROM vendor_schedule WHERE vendor_id = :vendor_id AND weekday = :weekday';
        $sth = DbProvider::getInstance()->prepare($sql);
        $sth->bindParam(':vendor_id', $this->id, PDO::PARAM_INT);
        $sth->bindParam(':weekday', $weekday, PDO::PARAM_INT);
        $sth->execute();
    }

    /**
     * Add Schedules.
     * @param $Schedules
     */
    public function addSchedules($Schedules)
    {
        foreach ($Schedules as $Schedule) {
            $Schedule->vendor_id = $this->id;
            if (!property_exists($Schedule, 'event_type') || $Schedule->event_type == 'opened') {
                $Schedule->save();
            }
        }
    }


}