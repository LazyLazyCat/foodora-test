<?php

include_once 'DbProvider.php';
include_once 'Vendor.php';

$type = $argv[1];
if (!empty($argv[2])) {
    $name = $argv[2];
}
$functionName = null;

if (is_null($type)) {
    echo 'You need set type as first parameter!';
    exit();
}

function checkName($name)
{
    if (is_null($name)) {
        echo 'You need set name of dump as second parameter!';
        exit();
    }
}

function checkFunctionName($functionName)
{
    if (is_null($functionName)) {
        checkType(null);
    }
}

function checkType($type)
{
    if (is_null($type)) {
        echo 'Your type is not valid! Please use: dump|updateFromDump|updateFromSpecialDay|deleteDump as first parameter';
        exit();
    }
}

switch ($type) {
    case 'dump':
        checkName($name);
        $functionName = 'dumpSchedule';
        break;
    case 'deleteDump':
        checkName($name);
        $functionName = 'deleteDump';
        break;
    case 'updateFromDump':
        checkName($name);
        $functionName = 'updateScheduleFromDump';
        break;
    case 'updateFromSpecialDay':
        $functionName = 'updateScheduleFromSpecialDays';
        break;
}

DbProvider::getInstance()->beginTransaction();
try {
    $Vendors = Vendor::getAll();
    foreach ($Vendors as $Vendor) {
        $Vendor->$functionName($name);
    }
} catch (Exception $e) {
    echo 'We had some exception!!';
    DbProvider::getInstance()->rollBack();
    exit();
}
DbProvider::getInstance()->commit();




