<?php

/**
 * Just db connection.
 * Class DbProvider
 */
class DbProvider
{
    /**
     * Name of host.
     * @var string
     */
    private static $host = 'localhost';
    /**
     * Name of DB table.
     * @var string
     */
    private static $dbname = 'foodora';
    /**
     * Current user.
     * @var string
     */
    private static $user = 'root';
    /**
     * Current password.
     * @var string
     */
    private static $password = 'root';
    /**
     * @var PDO
     */
    private static $instance;

    /**
     * @return PDO
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new PDO('mysql:host=' . self::$host . ';dbname=' . self::$dbname . ';charset=utf8mb4',
              self::$user, self::$password);
            static::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return static::$instance;
    }

    protected function __construct()
    {
    }

    private function __clone()
    {
    }
}